###################
CRUD Users with CodeIgniter
###################

Web Application developed with CodeIgniter Framework. The users can be created, readed, updated and deleted.
Each user has a name, email, phone, age and roles.
The email of user should be unique. There aren't two users with the same email.
A user can have many roles and a role can be associated to many users

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Please see the `installation section <https://codeigniter.com/user_guide/installation/index.html>`_
of the CodeIgniter User Guide.

After the installation of CodeIgniter, you can clone the project, run:

git clone https://Arivas16@bitbucket.org/Arivas16/userscodeigniter.git

You should to create the database from the file webapp.sql and configure your file in
webapp->application->config->database.php

Configure the project in your server and go to the browser to the URL http://localhost/webapp/
