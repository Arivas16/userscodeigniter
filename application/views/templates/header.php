<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD Users</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">

    <nav class="navbar navbar-default navbar-fixed-top">
	  	<div class="container">
	  		<ul class="nav navbar-nav">
		  		<li class="dropdown">
		  			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Usuarios <span class="caret"></span></a>
		  			<ul class="dropdown-menu">
			            <li>
				  			<a href="<?php echo site_url('users/create'); ?>">
				        		<span class="glyphicon glyphicon-plus"></span> Crear Usuario
				        	</a>
				        </li>
			            <li role="separator" class="divider"></li>
			            <li>
			            	 <a href="<?php echo site_url('users/index'); ?>">
					        	<span class="glyphicon glyphicon-list"></span> Lista de Usuarios
					        </a>
			            </li>
		          </ul>
	        	</li>
	        	<li class="dropdown">
		  			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog"></span> Roles <span class="caret"></span></a>
		  			<ul class="dropdown-menu">
			            <li>
				  			<a href="<?php echo site_url('roles/create'); ?>">
				        		<span class="glyphicon glyphicon-plus"></span> Crear Rol
				        	</a>
				        </li>
			            <li role="separator" class="divider"></li>
			            <li>
			            	 <a href="<?php echo site_url('roles/index'); ?>">
					        	<span class="glyphicon glyphicon-list"></span> Lista de Roles
					        </a>
			            </li>
		          </ul>
	        	</li>
        	</ul>
		</div>
	</nav>

	<div class="flash-message">
		<?php if($this->session->flashdata('msg')): ?>
			<p class="alert alert-success"><?php echo $this->session->flashdata('msg'); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		<?php endif; ?>
		<?php if($this->session->flashdata('error')): ?>
			<p class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		<?php endif; ?>
	</div> <!-- end .flash-message -->

    <div class="container">