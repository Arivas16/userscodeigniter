<div class="row">
    <div class="col-xs-12">
        <h2>Crear Usuario</h2>

        <div class="error text-danger">
            <?php echo validation_errors(); ?>
        </div>

        <?php echo form_open('users/create'); ?>

            <div class="form-group">
                <label for="name">Nombre</label>
                 <input type="text" name="name" minlength="3" class="form-control" placeholder="Nombre" required/>
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" placeholder="Email" required/>
            </div>

            <div class="form-group">
                <label for="phone">Teléfono</label>
               <input type="tel" name="phone" minlength="7" class="form-control" placeholder="Telefono" required/>
            </div>

            <div class="form-group">
                <label for="age">Edad</label>
               <input type="number" min="18" max="150" name="age" class="form-control" placeholder="Edad" required/>
            </div>

            <div class="form-group">
                <label for="roles">Roles del Usuario</label>
                <select name="roles[]" class="form-control" multiple required>
            		<?php foreach($roles as $row) { ?>
            			<option value="<?=$row['role_id']?>"><?=$row['description']?></option>
            		<?php } ?>
            	</select>
            </div>

            <button type="submit" class="btn btn-default">Guardar</button>

        </form>
    </div>
</div>