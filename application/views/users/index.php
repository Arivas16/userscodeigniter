<div class="row">
	<div class="col-xs-12">
		<h2>Lista de Usuarios</h2>
		<?php if ($users): ?>
		<table class="table table-bordered table-striped">
			<thead>
				<th>#</th>
				<th>Nombre</th>
				<th>Email</th>
				<th>Telefono</th>
				<th>Edad</th>
				<th>Roles</th>
				<th>Acciones</th>
			</thead>
			<tbody>
				<?php foreach ($users as $users_item): ?>
					<tr>
			        	<td><?php echo $users_item['user_id']; ?></td>
			            <td><?php echo $users_item['name']; ?></td>
			            <td><?php echo $users_item['email']; ?></td>
			            <td><?php echo $users_item['phone']; ?></td>
			            <td><?php echo $users_item['age']; ?></td>
			            <td>
				            <?php foreach ($users_item['roles'] as $rol): ?>
				            	<?php echo $rol; ?><br/>
				            <?php endforeach; ?>
				        </td>
			            <td>
				            <a href="<?php echo site_url('users/edit/'.$users_item['user_id']); ?>" class="btn btn-default btn-sm">
			          			<span class="glyphicon glyphicon-pencil"></span>
			        		</a>
			        		<a href="<?php echo site_url('users/delete/'.$users_item['user_id']); ?>" class="btn btn-default btn-sm">
			          			<span class="glyphicon glyphicon-remove"></span>
		        			</a>
		        		</td>
	        		</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php else: ?>
			<h2>No existen usuarios registrados</h2>
		<?php endif; ?>
	</div>
</div>