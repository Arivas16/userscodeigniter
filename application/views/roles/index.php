<div class="row">
	<div class="col-xs-12">
		<h2>Lista de Roles</h2>
		<?php if ($roles): ?>
		<table class="table table-bordered table-striped">
			<thead>
				<th>#</th>
				<th>Descripción</th>
				<th>Acciones</th>
			</thead>
			<tbody>
				<?php foreach ($roles as $row): ?>
					<tr>
			        	<td><?php echo $row['role_id']; ?></td>
			            <td><?php echo $row['description']; ?></td>
			            <td>
			            	<a href="<?php echo site_url('roles/edit/'.$row['role_id']); ?>" class="btn btn-default btn-sm">
			          			<span class="glyphicon glyphicon-pencil"></span>
			        		</a>
			        		<a href="<?php echo site_url('roles/delete/'.$row['role_id']); ?>" class="btn btn-default btn-sm">
			          			<span class="glyphicon glyphicon-remove"></span>
			        		</a>
		        		</td>
	        		</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php else: ?>
			<h2>No existen roles registrados</h2>
		<?php endif; ?>
	</div>
</div>