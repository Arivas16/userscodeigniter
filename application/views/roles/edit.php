<div class="row">
    <div class="col-xs-12">
		<h2>Crear Rol</h2>

		<div class="error text-danger">
			<?php echo validation_errors(); ?>
		</div>

		<?php echo form_open('roles/edit/'.$role_id); ?>

		    <div class="form-group">
		        <label for="description">Descripción</label>
		        <input type="text" minlength="3" name="description" class="form-control" value="<?php echo $rol['description']; ?>" required/>
		    </div>
		    <button type="submit" class="btn btn-default">Guardar</button>

		</form>
	</div>
</div>