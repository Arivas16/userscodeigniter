<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('roles_model');
        $this->load->helper('url_helper');
        $this->load->library('session');
    }


	public function index()
	{
		// Get all roles from database
		$data['title'] = 'Lista de Roles';
		// Call to function in Role model to get the roles registered
        $data['roles'] = $this->roles_model->get_roles();
		$this->load->view('templates/header');
        $this->load->view('roles/index', $data);
        $this->load->view('templates/footer');
	}

	public function valid_form(){
		// Specific rules to validate the form to create and update a role
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules(
	    		'description', 'Descripción', 'required|min_length[3]',
	    		array(
				    'required'      => 'La descripcion es obligatoria',
				    'min_length'    => 'La descripcion debe tener al menos 3 caracteres'
				)
	    	);

	    return $this->form_validation->run();

	}

	public function create()
	{
		// Create new role
		$this->session->set_flashdata('msg', null);
		$this->session->set_flashdata('error', null);

		$data['title'] = 'Crear Rol';

		// Call to function to validate the form
	    if ($this->valid_form() === FALSE)
	    {
	    	// If the form is invalid set flash message and show the form again
	    	$this->session->set_flashdata('error', 'Ocurrió un error creando el rol');

	        $this->load->view('templates/header');
	        $this->load->view('roles/create', $data);
	        $this->load->view('templates/footer');

	    }
	    else
	    {
	    	// If the form is valid save the new role
	    	$this->roles_model->set_roles();
	    	$this->session->set_flashdata('msg', 'Rol creado exitosamente');
			redirect('roles/index', 'refresh');
	    }
	}

	public function edit($role_id)
	{
		// Update a role stored in database
		$this->session->set_flashdata('msg', null);
		$this->session->set_flashdata('error', null);

		$data['title'] = 'Editar Rol';
		// Get the role information from database to show in form
        $data['rol'] = $this->roles_model->get_role_by_id($role_id);
        $data['role_id'] = $role_id;

        // Call to function to validate the form
	    if ($this->valid_form()=== FALSE)
	    {
	    	// If the form is invalid set flash message and show the form again
	    	$this->session->set_flashdata('error', 'Ocurrió un error actualizando el rol');

	        $this->load->view('templates/header');
	        $this->load->view('roles/edit', $data);
	        $this->load->view('templates/footer');

	    }
	    else
	    {
	    	// If the form is valid update the role
	    	$this->roles_model->update_role($role_id);
	    	$this->session->set_flashdata('msg', 'Rol actualizado exitosamente');
			redirect('roles/index', 'refresh');
	    }
	}

	public function delete($role_id)
	{
		// Delete a role stored at database
		$this->roles_model->delete_role($role_id);
		$this->session->set_flashdata('msg', 'Rol eliminado exitosamente');
		redirect('roles/index', 'refresh');
	}
}
