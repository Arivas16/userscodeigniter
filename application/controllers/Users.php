<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        // Load required libraries and models
        $this->load->model('users_model');
        $this->load->model('roles_model');
        $this->load->model('usersroles_model');
        $this->load->helper('url_helper');
        $this->load->library('session');
    }


	public function index()
	{
		// Get the registered users
		$data['title'] = 'Lista de Usuarios';
		// Load all users from database with user model method
        $data['users'] = $this->users_model->get_users();
		$this->load->view('templates/header');
        $this->load->view('users/index', $data);
        $this->load->view('templates/footer');
	}

	public function valid_form($email_unique){
		// Validation form with specific rules

		$this->load->helper('form');
	    $this->load->library('form_validation');

		$this->form_validation->set_rules(
				'name', 'Name', 'required|min_length[3]',
				array(
				    'required'      => 'El nombre es obligatorio',
				    'min_length'    => 'El nombre debe tener al menos 3 caracteres'
				)
        	);

	    if ($email_unique){
	    	$this->form_validation->set_rules(
	    		'email', 'Email', 'required|valid_email|is_unique[users.email]',
	    		array(
				    'required'      => 'El email es obligatorio',
				    'valid_email'   => 'El email no tiene el formato correcto',
				    'is_unique'		=> 'El email ya se encuentra asociado a otro usuario registrado'
				)
        	);
	    }else{
	    	 $this->form_validation->set_rules(
	    		'email', 'Email', 'required|valid_email',
	    		array(
				    'required'      => 'El email es obligatorio',
				    'valid_email'   => 'El email no tiene el formato correcto'
				)
        	);
	    }

	    $this->form_validation->set_rules(
	    		'phone', 'Phone', 'required|min_length[7]',
	    		array(
				    'required'      => 'El telefono es obligatorio',
				    'min_length'    => 'El telefono debe tener al menos 7 caracteres'
				)
			);
	    $this->form_validation->set_rules(
	    		'age', 'Age', 'required|numeric',
	    		array(
				    'required'      => 'La edad es obligatoria',
				    'numeric'    => 'La edad debe ser un numero'
				)
			);
	    $this->form_validation->set_rules(
	    		'roles[]', 'Roles', 'required',
	    		array(
				    'required'      => 'Debe asignarle al menos un rol al usuario'
				)
			);

	    return $this->form_validation->run();
	}

	public function create()
	{
		// Save an user in database
		$data['title'] = 'Crear Usuario';
		// Get the roles from database to show in select role form
        $data['roles'] = $this->roles_model->get_roles();

        /* Call valid_form method with param in true because is required that new email 
		   isn't registered with other user already stored
        */
	    if ($this->valid_form(TRUE) === FALSE)
	    {
	    	// If the form is invalid set flash message and show the form again
	    	$this->session->set_flashdata('error', 'Ocurrió un error creando el usuario');
	        $this->load->view('templates/header');
	        $this->load->view('users/create', $data);
	        $this->load->view('templates/footer');

	    }
	    else
	    {
	    	// If the form is valid save the new user
	        $user_id = $this->users_model->set_users();
	        /* For each role selected, call the users roles model to save the role 
	        with the new user stored in database */
	        foreach ($this->input->post('roles') as $rol) {
	        	$this->usersroles_model->set_users_roles($user_id, $rol);
	    	}
	    	// Set flash message and show the users list
	    	$this->session->set_flashdata('msg', 'Usuario creado exitosamente');
			redirect('users/index', 'refresh');
	    }
	}

	public function edit($user_id)
	{
		// Update user
		$data['title'] = 'Editar Usuario';
        $data['roles'] = $this->roles_model->get_roles();
        // Get the user information from database to show in form
        $data['user'] = $this->users_model->get_user_by_id($user_id);
        $data['user_id'] = $user_id;
        $valid_email = FALSE;

        // Validate if the email to update is different to the stored in database
        if ($this->input->post('email') != $data['user']['email']) {
        	// If is different is required validate that the new email will be unique
        	$valid_email = TRUE;
        }

        // Call the function to validate the form
	    if ($this->valid_form($valid_email) === FALSE)
	    {
	    	// If the form is invalid set flash message and show the form again
	    	$this->session->set_flashdata('error', 'Ocurrió un error actualizando el usuario');
	        $this->load->view('templates/header');
	        $this->load->view('users/edit', $data);
	        $this->load->view('templates/footer');

	    }
	    else
	    {
	    	// If the form is valid update the user
	        $this->users_model->update_user($user_id);
	        // Delete users roles associated to the user to save the new roles
	        $this->usersroles_model->delete_users_roles($user_id);
	        /* For each role selected, call the users roles model to save the role 
	        with the new user stored in database */
	        foreach ($this->input->post('roles') as $rol) {
	        	$this->usersroles_model->set_users_roles($user_id, $rol);
	    	}
	    	// Set flash message and show the users list
	    	$this->session->set_flashdata('msg', 'Usuario actualizado exitosamente');
			redirect('users/index', 'refresh');
	    }
	}

	public function delete($user_id)
	{
		// Delete an user stored at database
		$this->users_model->delete_user($user_id);
		$this->session->set_flashdata('msg', 'Usuario eliminado exitosamente');
		redirect('users/index', 'refresh');
	}
}
