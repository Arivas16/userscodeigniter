<?php
class UsersRoles_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function get_users_roles()
		{
			// Get all users_roles stored in database
			$query = $this->db->get('users_roles');
            return $query->result_array();
		}

		public function set_users_roles($user_id, $rol_id)
		{
			// Save a new user_role with a roleId and userId received from params
		    $data = array(
		        'user_id' => $user_id,
		        'role_id' => $rol_id
		    );

		    return $this->db->insert('users_roles', $data);
		}

		public function delete_users_roles($user_id){
			// Delete users_roles associated to a userId received from param
			return $this->db->delete('users_roles', array('user_id' => $user_id)); 
		}
}