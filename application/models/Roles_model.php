<?php
class Roles_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function get_roles()
		{
			// Get all roles registered in database
			$query = $this->db->get('roles');
            return $query->result_array();
		}

		public function set_roles()
		{
			// Save a new role with the info from create form
		    $this->load->helper('url');

		    $data = array(
		        'description' => $this->input->post('description'),
		    );

		    return $this->db->insert('roles', $data);
		}

		public function get_role_by_id($role_id)
		{
			// Get role info by RoleID
			$data = null;
			$query = $this->db->get_where('roles', array('role_id' => $role_id));
			$query = $query->result_array();
			foreach ($query as $role) {
				$data = $role;
			}
            return $data;
		}

		public function update_role($id)
		{
			// Update a role with the info from edit form
		    $this->load->helper('url');

		    $data = array(
		        'description' => $this->input->post('description'),
		    );

		    $this->db->where('role_id', $id);
			return $this->db->update('roles', $data);
		}

		public function delete_role($id)
		{
			// Delete a role by RoleID
			return $this->db->delete('roles', array('role_id' => $id));
		}
}