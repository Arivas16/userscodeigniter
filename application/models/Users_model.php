<?php
class Users_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function get_users()
		{
			// Load all users from database
			$data = array();
			$query = $this->db->get('users');
			$query = $query->result_array();
			/* For each user stored in database get associated users_roles and set a field roles
			   with the role's name */
			foreach ($query as $user) {
				$roles = $this->db->get_where('users_roles', array('user_id' => $user['user_id']));
				$user['roles'] = array();
				foreach ($roles->result_array() as $rol) {
					$roles_name = $this->db->get_where('roles', array('role_id' =>$rol['role_id']));
					foreach ($roles_name->result() as $row)
					{
						// Set field roles with the name of each role of users_roles registered for this user
				        $user['roles'][] = $row->description;
					}
				}
				$data[] = $user;
			}
            return $data;
		}

		public function get_user_by_id($user_id)
		{
			// Get user info by UserID
			$data = null;
			$query = $this->db->get_where('users', array('user_id' => $user_id));
			$query = $query->result_array();
			foreach ($query as $user) {
				$data = $user;
			}
            return $data;
		}

		public function set_users()
		{
			// Save a new user with the info from create form
		    $this->load->helper('url');

		    $data = array(
		        'name' => $this->input->post('name'),
		        'email' => $this->input->post('email'),
		        'phone' => $this->input->post('phone'),
		        'age' => $this->input->post('age')
		    );

		    $this->db->insert('users', $data);
        	return $this->db->insert_id();
		}

		public function update_user($id)
		{
			// Update an user with the info from edit form
		    $this->load->helper('url');

		    $data = array(
		        'name' => $this->input->post('name'),
		        'email' => $this->input->post('email'),
		        'phone' => $this->input->post('phone'),
		        'age' => $this->input->post('age')
		    );

		    $this->db->where('user_id', $id);
			return $this->db->update('users', $data);
		}

		public function delete_user($id)
		{
			// Delete an user by UserID
			return $this->db->delete('users', array('user_id' => $id));
		}
}